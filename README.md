# vsmile

based on https://archive.org/details/no-intro_romsets (20230227-190033)

ROM not added:
```sh
❯ tree -hs --dirsfirst                                                                                                                                                                                                                                                                                                         sam. 20 mai 2023 13:08:55
[  11]  .
├── [3.0M]  Action Mania (USA) (V.Smile Motion).zip
├── [3.0M]  Baby Einstein (USA) (V.Smile Baby) (Alt).zip
├── [3.2M]  Baby Einstein (USA) (V.Smile Baby).zip
├── [7.0M]  Cars 2 (USA) (V.Smile Motion).zip
├── [3.7M]  NASCAR Academy - Race Car Superstar (USA) (V.Smile Motion).zip
├── [3.9M]  Pooh's Hundred Acre Wood Adventure (USA) (V.Smile Baby).zip
├── [8.2M]  Shrek Forever After (USA) (V.Smile Motion).zip
├── [8.2M]  Super WHY! - The Beach Day Mystery (USA) (V.Smile Motion).zip
└── [3.1M]  Wiggles, The - It's Wiggle Time! (USA).zip

1 directory, 9 files
```
